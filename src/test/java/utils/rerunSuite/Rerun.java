package utils.rerunSuite;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Rerun implements IRetryAnalyzer {

    int minAttemptsCounter = 0, maxRetryLimit = 2;

    public boolean retry(ITestResult result) {
        if (!result.isSuccess()) {
            if (minAttemptsCounter < maxRetryLimit) {
                minAttemptsCounter++;
                result.setStatus(result.FAILURE);
                TestListener testListener = new TestListener();
                testListener.onTestFailure(result);
                return true;
            }
        }else {
            result.setStatus(result.SUCCESS);
        }
        return false;
    }
}
