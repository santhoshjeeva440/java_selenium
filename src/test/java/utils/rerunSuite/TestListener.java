package utils.rerunSuite;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import utils.log.Log;
import utils.extendReport.ExtendReport;
import java.util.Objects;

import static utils.extendReport.ExtendReportManager.getTest;

public class TestListener extends driver.Browser implements ITestListener {

    private static String getTestName(ITestResult result) {

        return result.getMethod().getConstructorOrMethod().getName();

    }

    @Override
    public void onTestStart(ITestResult result) {

        Log.info(getTestName(result) + "Test is Starting");
    }

    @Override
    public void onTestSuccess(ITestResult result) {

        Log.info(getTestName(result) + "Test is Starting");
        getTest().log(Status.PASS, "Test is Passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {

        Log.info(getTestName(result) + " test is failed.");
        String base64Screenshot =
                "data:image/png;base64," + ((TakesScreenshot) Objects.requireNonNull(driver)).getScreenshotAs(OutputType.BASE64);
        getTest().log(Status.FAIL, "Test Failed",
                getTest().addScreenCaptureFromBase64String(base64Screenshot).getModel().getMedia().get(0));
    }

    @Override
    public void onTestSkipped(ITestResult result) {

        Log.info(getTestName(result) + " test is skipped.");
        getTest().log(Status.SKIP, "Test Skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

        Log.info("Test failed but it is in defined success ratio " + getTest());
    }

    @Override
    public void onStart(ITestContext context) {

        Log.info("I am in onStart method " + context.getName());
        context.setAttribute("WebDriver", this.driver);
    }

    @Override
    public void onFinish(ITestContext context) {

        Log.info("I am in onFinish method " + context.getName());
        //Do tier down operations for ExtentReports reporting!
        ExtendReport.createReport().flush();
    }
}
