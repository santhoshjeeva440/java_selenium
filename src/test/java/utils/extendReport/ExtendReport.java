package utils.extendReport;

import baseClass.Configuration;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.aeonbits.owner.ConfigFactory;

public class ExtendReport {

    public static final ExtentReports extentReports = new ExtentReports();

    public static Configuration config = ConfigFactory.create(Configuration.class);

    public synchronized static ExtentReports createReport(){

        ExtentSparkReporter reporter = new ExtentSparkReporter("./target/extent-reports/extent-report.html");
        reporter.config().setReportName("Automation Suite Report");
        extentReports.attachReporter(reporter);
        extentReports.setSystemInfo("Environment Name", config.environment());
        extentReports.setSystemInfo("Browser Name", config.browserName());
        return extentReports;
    }
}
